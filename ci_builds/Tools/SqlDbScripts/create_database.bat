sqlcmd -b -S %1 -i %2 -v DatabaseName=%3 -U %4 -P %5

IF %ERRORLEVEL% == 0 GOTO OK
echo ##teamcity[buildStatus status='FAILURE' text='{build.status.text} in compilation']
EXIT /B 1
:OK